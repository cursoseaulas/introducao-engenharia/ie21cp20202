#include <Servo.h>
#include <IRremote.h>

#define LDR A5 // Fotoresistor
#define IR 4 // Infra vermelho
#define SR1 9 // Servo direção
#define SR2 10 // Servo profundidade
#define SR3 2 // Servo profundidade
#define MT1 5 //Motor1
#define MT2 6 //Motor2
#define LED1 13 //Led noturno
#define LED2 12 //
#define LED3 11 //
#define TRIG 8 //Sensor colisão
#define ECHO 7 //
#define ALAR1 A3 //Led colisão
#define INC 3 //Sendo inclinação
#define ALAR2 A4 //Led inclinação

Servo servo1; //Objeto servo
Servo servo2;
Servo servo3;

IRrecv receptor(IR); 
decode_results resultado;
bool ligado = false;


void setup()
{
  servo1.attach(SR1);
  servo2.attach(SR2);
  servo3.attach(SR3);
  receptor.enableIRIn();
  Serial.begin(9600);
  
  pinMode(MT1, OUTPUT);//Motores
  pinMode(MT2, OUTPUT);
  
  pinMode(LED1, OUTPUT);//Lâmpadas de posição
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  
  pinMode(TRIG, OUTPUT);//Sensor de colisão
  pinMode(ECHO, INPUT);
  digitalWrite(TRIG, LOW);
  pinMode(ALAR1, OUTPUT);
  
  pinMode(INC, INPUT);//Sensor de inclinação
  pinMode(ALAR2, OUTPUT);
  
  servo1.write(90);
  servo2.write(90);
  servo3.write(90);
  
}
			//FD609F DIREITA 45º
			//FD20DF ESQUERDA 135º
			//FD50AF SUBIR
			//FD10EF DESCER
			//FD807F PRA FRENTE
			//FD08F7 1
			//FD8877 2
			//FD48B7 3
			//ligado == 30 +- 650rpm
			//vel1   == 50 +- 1500rpm
			//vel2   == 120 +- 2600rpm
			//vel3 	 == 220 +- 4800rpm

void loop() {
  
  int leitura = analogRead(LDR); // Luz noturna
  
  bool inclinado = digitalRead(INC);//Alarme inclinação
  
  	digitalWrite(TRIG, HIGH); //Alarme de colisão
	delayMicroseconds(10);
	digitalWrite(TRIG, LOW);
  
	int tempo = pulseIn(ECHO, HIGH);  
	float dist = (tempo * 343.0)/20000; 
  
  if (dist < 70 && dist >= 50){ 	//Alarme de colisão
      digitalWrite(ALAR1, HIGH);
      delay(800);
      digitalWrite(ALAR1, LOW);
      delay(800);
  } 
  if (dist < 50 && dist >=25){ 	//Alarme de colisão
      digitalWrite(ALAR1, HIGH);
      delay(400);
      digitalWrite(ALAR1, LOW);
      delay(400);
  } 
  if (dist < 25){ 	//Alarme de colisão
      digitalWrite(ALAR1, HIGH);
      delay(200);
      digitalWrite(ALAR1, LOW);
      delay(200);
  } 
  else{//Alrme de colisão
    digitalWrite(ALAR1, LOW);
  }	
  
  if (!inclinado){ //Alarme de inclinação
	  digitalWrite(ALAR2, HIGH);
      delay(200);
      digitalWrite(ALAR2, LOW);
      delay(200);
  }
  else{//Alarme de inclinaçã
    digitalWrite(ALAR2, LOW);
  }  

  
  
  if(leitura>140){				//Luz Noturna
   	digitalWrite(LED1, HIGH);
    digitalWrite(LED2, HIGH); 
    digitalWrite(LED3, HIGH); 
  }
  else{
    digitalWrite(LED1, LOW);
    digitalWrite(LED2, LOW); 
    digitalWrite(LED3, LOW); 
  }								//Luz noturna
  	  
	if (receptor.decode(&resultado)) {
      Serial.println(resultado.value, HEX);
	
      	switch (resultado.value) {
        
        case 0xFD807F : // frente
        
            analogWrite(MT1, 30);
  			analogWrite(MT2, 30);
        	servo1.write(90);
            servo2.write(90);
            servo3.write(90);
  			delay(10);
          break;
          
         case 0xFD08F7 : // vel 1          
            analogWrite(MT1, 50);
  			analogWrite(MT2, 50);
        	servo1.write(90);
            servo2.write(90);
            servo3.write(90);
  			delay(10);
          break;
                    
         case 0xFD8877 : // vel 2          
            analogWrite(MT1, 110);
  			analogWrite(MT2, 110);
        	servo1.write(90);
            servo2.write(90);
            servo3.write(90);
  			delay(10);
          break;
          
        case 0xFD48B7 : // vel 3         
            analogWrite(MT1, 220);
  			analogWrite(MT2, 220);
          	servo1.write(90);
            servo2.write(90);
            servo3.write(90);
  			delay(10);
          break;
             
        case 0xFD609F : // esquerda
        	analogWrite(MT1, 50);
  			analogWrite(MT2, 50);
        	servo1.write(135);
  			delay(10);
        break;
        
       case 0xFD20DF : // direita  
        	analogWrite(MT1, 50);
  			analogWrite(MT2, 50);
        	servo1.write(45);
  			delay(10);
        break;
          
       case 0xFD50AF : // subir 
        	analogWrite(MT1, 50);
  			analogWrite(MT2, 50);
        	servo1.write(90);
        	servo2.write(45);
        	servo3.write(135);
  			delay(10);
        break;
          
       case 0xFD10EF : // descer
        	analogWrite(MT1, 50);
  			analogWrite(MT2, 50);
        	servo1.write(90);
        	servo2.write(135);
        	servo3.write(45);
  			delay(10);
        break;
          
       default : // neutro
        	analogWrite(MT1, 0);
  			analogWrite(MT2, 0);
        	servo1.write(90);
        	servo2.write(90);
        	servo3.write(90);
  			delay(10);
        break;
          
      }
        
        receptor.resume();       
 }
}
